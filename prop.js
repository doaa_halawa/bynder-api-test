import { Counter } from "k6/metrics";
import { check } from "k6";

export let base_url = `${__ENV.BASE_URL}`;
export let api_key = "4a6d1e48aad8b189a033110c1f664ce6";
export let invalid_api_key = "123d1e48aad8b189a033110c1f664123";
export let invalid_guest_session_ID = "123d1e48aad8b189a033110c1f664123";

let failedTestCases = new Counter("failedTestCases");

export let assert = function(result, name) {
  check(null, { [name]: result }); // to record a check
  failedTestCases.add(!result);
  return result;
};
