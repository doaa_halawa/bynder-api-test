import http from "k6/http";
import * as p from "./prop.js";

export function getTopRatedMoviesRequest(api_key, extra_parameters = "") {
  return http.get(
    `${p.base_url}/movie/top_rated?api_key=${api_key}&${extra_parameters}`
  );
}

export function getGuestSessionRequest(api_key) {
  return http.get(
    `${p.base_url}/authentication/guest_session/new?api_key=${api_key}`
  );
}

export function rateMovieRequest(api_key, payload, movieID, guest_session_id) {
  return http.post(
    `${p.base_url}/movie/${movieID}/rating?api_key=${api_key}&guest_session_id=${guest_session_id}`,
    JSON.stringify(payload),
    {
      headers: {
        "Content-Type": "application/json;charset=utf-8",
      },
      tags: {
        name: `${p.base_url}/movie/${movieID}/rating?api_key=${api_key}&guest_session_id=${guest_session_id}`,
      },
    }
  );
}
