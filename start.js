import rateMovie from "./api/rateMovie.js";
import topRated from "./api/topRated.js";
import { functionalTestsOption } from "./prop.js";

export let options = functionalTestsOption;

export default function() {
  rateMovie();
  topRated();
}
