import { check, sleep, group, fail } from "k6";
import { getTopRatedMoviesRequest } from "../request.js";
import { api_key } from "../prop.js";

export let options = {
  vus: 400,
  duration: "60s",
};

let count = 0;
export default function() {
  count += 1;
  group("Top Rated", (_) => {
    group("GET movie/top_rated returns list of top rateted movies", (_) => {
      let response = getTopRatedMoviesRequest(api_key);

      check(response, {
        "status code must be 200": (res) => res.status === 200,
      }) || fail("get top rated movies request failed");

      sleep(1);
    });
  });
}
