FROM loadimpact/k6:0.26.1
ENV SCRIPT start.js
COPY . /test
WORKDIR /test
# Override the entry point of the base k6 image
ENTRYPOINT []
CMD ["sh", "-c", "k6 run $SCRIPT --http-debug=\"full\""]