import { group } from "k6";
import {
  rateMovieRequest,
  getGuestSessionRequest,
  getTopRatedMoviesRequest,
} from "../request.js";
import { assert, api_key, invalid_guest_session_ID } from "../prop.js";

let guestSessionID;
let movieID;

export default function() {
  group("Rate Movie", (_) => {
    group(
      "POST /movie/${movieID}/rating returns 201 when we rate a movie",
      (_) => {
        //Get movie ID
        let movieResponse = getTopRatedMoviesRequest(api_key);
        movieID = JSON.parse(movieResponse.body.replace("/", "")).results[0].id;

        //Get guest session ID
        let guestSessionResponse = getGuestSessionRequest(api_key);
        assert(guestSessionResponse.status === 200, "status code must be 200");
        guestSessionID = JSON.parse(guestSessionResponse.body.replace("/", ""))
          .guest_session_id;

        //Rate a movie
        let payload = {
          value: 8.5,
        };
        let rateMovieResponse = rateMovieRequest(
          api_key,
          payload,
          movieID,
          guestSessionID
        );
        assert(rateMovieResponse.status === 201, "status code must be 201");
        assert(
          rateMovieResponse.json().status_message === "Success.",
          "Message should be success"
        );
      }
    );

    group(
      "POST /movie/${movieID}/rating returns 401 with incorrect guest session id",
      (_) => {
        let payload = {
          value: 8.5,
        };
        let rateMovieResponse = rateMovieRequest(
          api_key,
          payload,
          movieID,
          invalid_guest_session_ID
        );
        assert(rateMovieResponse.status === 401, "status code must be 401");
      }
    );
  });
}
