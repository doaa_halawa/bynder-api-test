import { group } from "k6";
import { getTopRatedMoviesRequest } from "../request.js";
import { assert, invalid_api_key, api_key } from "../prop.js";

export default function() {
  group("Top Rated Movies", (_) => {
    group("GET movie/top_rated returns list of top rateted movies", (_) => {
      let response = getTopRatedMoviesRequest(api_key);
      assert(response.status === 200, "status code must be 200");
    });

    group(
      "GET movie/top_rated returns Invalid API key if api key is incorrect",
      (_) => {
        let response = getTopRatedMoviesRequest(invalid_api_key);
        assert(response.status === 401, "status code must be 401");
        assert(
          response.json().status_message ===
            "Invalid API key: You must be granted a valid key.",
          "Reason should be invalid API key"
        );
        assert(
          response.json().success === false,
          "Request should not be successed"
        );
      }
    );

    group(
      "GET movie/top_rated with number of pages returns list of top rateted movies for specific page",
      (_) => {
        //Get number of pages
        let listMoviesResponse = getTopRatedMoviesRequest(api_key);
        let pageNumber = JSON.parse(listMoviesResponse.body.replace("/", ""))
          .total_pages;

        //Get top rated movies for specific page number
        let response = getTopRatedMoviesRequest(api_key, `page=${pageNumber}`);
        assert(response.status === 200, "status code must be 200");
        assert(
          response.json().page === pageNumber,
          "Request should return proper page"
        );
      }
    );
  });
}
