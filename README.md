# Bynder API Tests

This project contains test cases for bynder api endpoints using k6.

# Install

`brew install k6`

# Why K6

- This is a high performance golang based framework. Framework is very mature and developers respond quickly to your query
- Developer friendly APIs and scripting in Javascript
- You can execute your functional tests, smoke tests and use the same scenarios to load testing by increasing the
  number of virtual users and iterations
- Provides functionality that will help us to analyze the http requests performance and run tests with a small
  amount of load to continuously monitor the performance of our production environment.
- Provides plugins to visualize the test results

# Files

- `request.js`

  This file contains request wrapper for different endpoints that will be used multiple times in the tests and common functions.

- `prop.js`

  This file contains all the global parameters

- `start.js`

  Running this file will run all the tests cases

# Running

Functional tests are run with only one virtual user and iterations

First you need to define base URL (You can change it to whatever enviroment you want to run these tests)
`export BASE_URL=https://api.themoviedb.org/3`

`k6 run start.js`

`k6 run start.js --vus 1 --iterations 1`

If you wish to see all the info about the http endpoint

`k6 run start.js --http-debug="full"`

If you wish you run using docker container

`docker-compose build`

`docker-compose up`

# Running Load Test

if you have access to the instance then on the home directory

    export BASE_URL=https://api.themoviedb.org/3
    cd ~
    cd bynder-api-tests/

Before running the performance tests run some functional tests to make sure you're well setup

    k6 run api/topRated.js --http-debug="full"

Modify `performance/topRated.js` for number of virtual users and duration.
You can keep the number of iterations very high.
If all the functional tests pass then run the below command

    k6 run performance/topRated.js

You can change `topRated.js` file for number of virtual users, duration.

    export let options = {
      vus: 3000,
      duration: "1800s",
      iterations: 450000,
    };
